<?php
session_start();
require_once "mainClass.php";
?>
<!DOCTYPE html>
<html lang="pl">

<?php
MainClass::return_head("Nie_Pastebin", "style_signup.css");
?>

<body>
    <form action="signin.php" method="post">
        <ul class="signup">
            <li>Nazwa użytkownika: </li>
            <li><input type="text" name="name" id=""></li>
            <li>Hasło:</li>
            <li><input type="password" name="pass" id=""></li>
            <?php
            if (isset($_SESSION['login_warning'])) {
                echo $_SESSION['login_warning'];
            }
            ?>
            <li><input type="submit" value="Zaloguj"></li>
        </ul>
    </form>

</body>

</html>