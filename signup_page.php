<?php
session_start();
require_once "mainClass.php";
?>
<!DOCTYPE html>
<html lang="pl">

<?php
MainClass::return_head("Nie_Pastebin", "style_signup.css");
?>

<body>
    <form action="signup.php" method="post">
        <ul class="signup">
            <li>Nazwa użytkownika: </li>
            <li><input type="text" name="name" id="" placeholder="Nazwa użytkownika"></li>
            <li>Email: </li>
            <li><input type="email" name="email" id="" placeholder="Email"></li>
            <li>Hasło:</li>
            <li><input type="password" name="pass" id="" placeholder="Hasło"></li>
            <li>Powtórz hasło:</li>
            <li><input type="password" name="pass_rep" id="" placeholder="Powtórz hasło"></li>
            <?php
            if (isset($_SESSION['signin_warning'])) {
                echo $_SESSION['signin_warning'];
            }
            ?>
            <li><input type="submit" value="Zarejestruj się"></li>
        </ul>
    </form>

</body>

</html>