<?php
class MainClass
{

    public static function db_connect()
    {
        require_once "config.php";
        $connection = new mysqli($host, $db_user, $db_password, $db_name);
        return $connection;
    }

    public static function restricted_redirect($site)
    {
        if ($_SESSION['logged_in'] == False) {
            header('Location: ' . $site);
        }
    }

    public static function check_newly_created_account($site)
    {
        if ($_SESSION['new_user'] == False) {
            header('Location: ' . $site);
        }
    }

    public static function return_head($sitename, $stylesheet)
    {
        echo
        '<head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>' . $sitename . '</title>
                <link rel="stylesheet" href="static/' . $stylesheet . '">
            </head>';
    }
}
