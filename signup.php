<?php
session_start();
require_once "mainClass.php";
$connection = MainClass::db_connect();

if ($connection->connect_errno == 0) {
    $userName = htmlentities($_POST['name'], ENT_QUOTES, "UTF-8");
    $userEmail = htmlentities($_POST['email'], ENT_QUOTES, "UTF-8");
    $userPassword = htmlentities($_POST['pass'], ENT_QUOTES, "UTF-8");
    $userPassword_Repeated = htmlentities($_POST['pass_rep'], ENT_QUOTES, "UTF-8");
    $validation_failed = FALSE;
    if (is_numeric($userName[0])) {
        session_unset();
        $validation_failed = TRUE;
        $_SESSION['signin_warning'] = 'Nazwa użytkownika nie może zaczynać się od cyfry!';
    }
    if (strlen($userPassword) < 8) {
        session_unset();
        $validation_failed = TRUE;
        $_SESSION['signin_warning'] = 'Hasło musi mieć przynajmniej 8 znaków!';
    }
    if ($userPassword != $userPassword_Repeated) {
        session_unset();
        $validation_failed = TRUE;
        $_SESSION['signin_warning'] = 'Hasła muszą być identyczne!';
    }

    $check_if_name_is_used = sprintf(
        "SELECT * FROM users WHERE userName='%s'",
        mysqli_real_escape_string($connection, $userName)
    );

    if ($result = $connection->query($check_if_name_is_used)) {
        if ($result->num_rows > 0) {
            session_unset();
            $validation_failed = TRUE;
            $_SESSION['signin_warning'] = 'Istnieje już użytkownik o takiej nazwie!';
        }
    }

    $check_if_email_is_used = sprintf(
        "SELECT * FROM users WHERE userEmail='%s'",
        mysqli_real_escape_string($connection, $userEmail)
    );

    if ($result = $connection->query($check_if_email_is_used)) {
        if ($result->num_rows > 0) {
            session_unset();
            $validation_failed = TRUE;
            $_SESSION['signin_warning'] = 'Istnieje już użytkownik o takim adresie email!';
        }
    }

    $sql = sprintf(
        "INSERT into users (userName, userEmail, userPassword) values ('%s', '%s', '%s')",
        mysqli_real_escape_string($connection, $userName),
        mysqli_real_escape_string($connection, $userEmail),
        mysqli_real_escape_string($connection, $userPassword)
    );
    if ($validation_failed === FALSE) {
        if ($connection->query($sql) === TRUE) {
            session_unset();
            $_SESSION['user'] = $user;
            $_SESSION['logged_in'] = true;
            $_SESSION['new_user'] = true;
            header('Location: signed_up.php');
        } else {
            session_unset();
            $_SESSION['signin_warning'] = 'Wystąpił błąd przy wysyłaniu danych!';
            header('Location: signup_page.php');
        }
    } else {
        header('Location: signup_page.php');
    }


    $connection->close();
}
