<?php
session_start();
require_once "interfaceClass.php";
?>
<!DOCTYPE html>
<html lang="pl">

<?php
InterfaceClass::restricted_redirect("index.php");
InterfaceClass::return_head("Nie_Pastebin", "style.css");
?>

<body>
    <?php
    InterfaceClass::return_header();
    ?>
    <div class="content">
        <?php
        echo 'Witaj, ' . $_SESSION["user"];
        ?>
    </div>
    <?php
    InterfaceClass::return_footer();
    ?>
</body>

</html>