<?php
session_start();
require_once "mainClass.php";
$connection = MainClass::db_connect();
if ($connection->connect_errno == 0) {

    $userName = htmlentities($_POST['name'], ENT_QUOTES, "UTF-8");
    $userPassword = htmlentities($_POST['pass'], ENT_QUOTES, "UTF-8");
    $sql = sprintf(
        "SELECT * FROM users WHERE userName='%s' AND userPassword='%s'",
        mysqli_real_escape_string($connection, $userName),
        mysqli_real_escape_string($connection, $userPassword)
    );

    if ($result = $connection->query($sql)) {
        if ($result->num_rows > 0) {

            $data = $result->fetch_assoc();
            $id = $data['userId'];
            $user = $data['userName'];
            $email = $data['userEmail'];
            $pass = $data['userPassword'];
            session_unset();
            $_SESSION['user'] = $user;
            $_SESSION['logged_in'] = true;
            header('Location: index.php');
            $result->close();
        } elseif ($result = $connection->query("SELECT * FROM users WHERE userName='$userName'")) {
            if ($result->num_rows == 0) {
                session_unset();
                $_SESSION['login_warning'] = 'Nie istnieje taki użytkownik!';
                header('Location: signin_page.php');
            } else {
                session_unset();
                $_SESSION['login_warning'] = 'Niepoprawne hasło!';
                header('Location: signin_page.php');
            }
        }
    }
    $connection->close();
}
