<?php
require_once "mainClass.php";

class InterfaceClass extends MainClass
{
    static public function return_header()
    {
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
            $buttons = '<li><div class="dropdown">
            <button class="dropbtn">' . $_SESSION["user"] . '
              <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content">
              <a href="user_detail_page.php">Profil</a>
              <a href="logout.php">Wyloguj</a>
            </div></li>';
        } else {
            $buttons = '<li class="nav_sign"><a href="signin_page.php" class="btn_sign_in">Logowanie</a></li>
            <li class="nav_sign"><a href="signup_page.php" class="btn_sign_up">Utwórz konto</a></li>';
        }
        echo
        '<nav>
            <ul>
                <li><a href="#">Logo</a></li>
                <li><a href="index.php">Nie-Pastebin</a></li>
                <li class="filler"></a></li>
                ' . $buttons . '
                </div>
            </ul>
        </nav>';
    }

    static public function return_footer()
    {
        echo
        '<footer>
            <ul>
                <li><a href="#">Autorzy:</a></li>
                <li><a href="#">Stanisław Rusiłowicz</a></li>
                <li><a href="#">Bartosz Winkiewicz</a></li>
                <li class="filler"></li>
                <li><a href="#">Nie-Pastebin 2022</a></li>
            </ul>
        </footer>';
    }
}
