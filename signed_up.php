<?php
session_start();
require_once "mainClass.php";
MainClass::check_newly_created_account("index.php")
?>
<!DOCTYPE html>
<html lang="pl">

<?php
MainClass::return_head("Nie_Pastebin", "style_signup.css");
?>

<body>
    <form>
        <ul>
            <li>Pomyślnie dodano użytkownika!</li>
            <li><a href="index.php">Powrót do strony głównej</a></li>
        </ul>
    </form>
</body>

</html>